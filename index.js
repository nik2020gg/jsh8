let paragraphs = document.querySelectorAll("p");
paragraphs.forEach((paragraph) => {
  paragraph.style.backgroundColor = "#ff0000";
});

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
optionsList.childNodes.forEach((node) => {
  console.log(node.nodeName, node.nodeType);
});

let testParagraph = document.querySelector(".testParagraph");
testParagraph.textContent = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");
let links = mainHeader.querySelectorAll("a");
links.forEach((link) => {
  link.classList.add("nav-item");
  console.log(link);
});

let sectionTitles = document.querySelectorAll(".section-title");
sectionTitles.forEach((title) => {
  title.classList.remove("section-title");
});
